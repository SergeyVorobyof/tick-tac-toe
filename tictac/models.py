from django.db import models
from django.utils import timezone

class Game_1(models.Model):
    author = models.ForeignKey('auth.User', related_name='games_as_owner')
    opponent = models.ForeignKey('auth.User', related_name='games_as_opponent', null=True)
    status = models.IntegerField(default=1)
    title = models.CharField(default='Game', max_length=100)
    board_state = models.CharField(default='_________', max_length=1000000)
    author_coords = models.CharField(default='', max_length=1000000)
    opponent_coords = models.CharField(default='', max_length=1000000)
    length_board = models.IntegerField(default = 3)
    win_comb = models.IntegerField(default = 3)