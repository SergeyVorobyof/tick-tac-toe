from django import template

register = template.Library()

@register.filter(name='get')
def get(value, index):
    return value[index]