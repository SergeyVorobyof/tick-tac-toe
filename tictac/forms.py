from django import forms
from .models import Game_1

class StartGame(forms.ModelForm):

	class Meta:
		model = Game_1
		fields = ('title', 'win_comb',  )
