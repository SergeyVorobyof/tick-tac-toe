from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^start-game/$', views.start_game, name = 'start_game'),
    url(r'^game/(?P<pk>[0-9]+)/$', views.my_games, name = 'my_games'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^register/$', views.register, name='register'),
    url(r'^$', views.game_list, name = 'game_list'),
    url(r'^game/(?P<pk>[0-9]+)/join/$', views.game_join, name='game_join'),
]
