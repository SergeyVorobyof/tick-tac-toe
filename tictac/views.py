from django.shortcuts import render
from django.utils import timezone
from .models import Game_1
from .forms import StartGame
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.context_processors import csrf
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django.shortcuts import render_to_response
from django.utils import timezone


def start_game(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            form = StartGame(request.POST)
            if form.is_valid():
                game = form.save(commit=False)
                game.author = request.user
                game.status = 0
                game.save()
                return redirect('tictac.views.my_games', pk=game.pk)
        else:
            form = StartGame()
        return render(request, 'tictac/start_game.html', {'form': form})
    return redirect('tictac.views.login')

def check(somebody_goes, cell, y, win_comb):
#    win_comb = ((0,1,2),(3,4,5),(6,7,8),(0,3,6),(1,4,7),(2,5,8),(0,4,8),(2,4,6))
#    for each in win_comb:
#        if board_state[each[0]] == board_state[each[1]] == board_state[each[2]] and board_state[each[0]] != '_':
#            return board_state[each[0]]
#    if not '_' in board_state:
#        return True
#    return False
    count = 1
    i = 0
    j = 0
    condition_left = True
    condition_right = True
    print('fiunction: check()')
    print(somebody_goes)
    while i < len(somebody_goes):
        somebody_goes[i] = int(somebody_goes[i])
        i += 1

    print('fiunction: check() step A')
    i = 0
    y_coords_somebody = []
    while i < len(somebody_goes):
        if somebody_goes[i] == cell and i % 2 == 0:
            y_coords_somebody.append(somebody_goes[i+1])
        i+=1

    print('fiunction: check() step B')
    i = 0
    cell_coords_somebody = []
    while i < len(somebody_goes):
        if somebody_goes[i] == y and i % 2 == 1:
            cell_coords_somebody.append(somebody_goes[i-1])
        i+=1


    i = 1
    j = 0
    print('fiunction: check() step 1')
    while i <= win_comb + 1:
        if  y + i in y_coords_somebody and condition_right:
            count += 1
        else:
            condition_right = False
        if  y - i in y_coords_somebody and condition_left:
            count += 1
        else:
            condition_left = False
        print(count)
        if not condition_right and not condition_left:
            if count == win_comb:
                return True
            else:
                count = 1
                i = 1
                condition_right = True
                condition_left = True
                break
        i+=1

    print('fiunction: check() step 2')
    while i <= win_comb+1:
        if cell + i in cell_coords_somebody and condition_right:
            count += 1
        else:
            condition_right = False
        if cell - i in cell_coords_somebody and condition_left:
            count += 1
        else:
            condition_left = False
        print(count)
        if not condition_right and not condition_left:
            if count == win_comb:
                return True
            else:
                count = 1
                i = 1
                condition_right = True
                condition_left = True
                break
        i+=1

    print('fiunction: check() step 3')
    while i <= win_comb + 1:
        if cell + i in cell_coords_somebody and y + i in y_coords_somebody and condition_right:
            count += 1
        else:
            condition_right = False
        if cell - i in cell_coords_somebody and y - i in y_coords_somebody and condition_left:
            count += 1
        else:
            condition_left = False
        print(count)
        if not condition_right and  not condition_left:
            if count == win_comb:
                return True
            else:
                count = 1
                i = 1
                condition_right =True
                condition_left = True
                break
        i+=1

    print('fiunction: check() step 4')
    while i <= win_comb+1:
        if cell + i in cell_coords_somebody and y - i in y_coords_somebody and condition_right:
            count += 1
        else:
            condition_right = False
        if  cell - i in cell_coords_somebody and y + i in y_coords_somebody and condition_left:
            count += 1
        else:
            condition_left = False
        print(count)
        if not condition_right and not condition_left:
            if count == win_comb:
                return True
            else:
                count = 1
                i = 1
                condition_right = True
                condition_left = True
                break
        i += 1
    return False

class Status:
    without_opponent = 0
    author_goes = 1
    opponent_goes = 2
    game_finished = 3

def my_games(request, pk):
    game = get_object_or_404(Game_1, pk=pk)
    temp_array = []
    temp_array_2 = []
    i = 0
    j = 0
    temp = 0
    while i < len(game.author_coords):
        if game.author_coords[i] == ',':
            temp_array.append(game.author_coords[j:i])
            j = i + 1
        if i == len(game.author_coords) - 1:
            temp_array.append(game.author_coords[j:len(game.author_coords)])
        i+=1
    print('temp array first')
    print (temp_array)
    temp_author_coords = []
    i = 0
    while i < len(temp_array):
        x = []
        x.append(temp_array[i])
        x.append(temp_array[i+1])
        temp_author_coords.append((','.join(x)))
        x.clear()
        i+=2

    i = 0
    j = 0
    temp = 0
    while i < len(game.opponent_coords):
        if game.opponent_coords[i] == ',':
            temp_array_2.append(game.opponent_coords[j:i])
            j = i + 1
        if i == len(game.opponent_coords) - 1:
            temp_array_2.append(game.opponent_coords[j:len(game.opponent_coords)])
        i+=1

    temp_opponent_coords = []
    i = 0
    while i < len(temp_array_2):
        x = []
        x.append(temp_array_2[i])
        x.append(temp_array_2[i+1])
        temp_opponent_coords.append((','.join(x)))
        x.clear()
        i+=2
    print ('TEMP_OPPONENT_COORDs')
    print(temp_opponent_coords)

    print ('TEMP_ARRAY')
    print (temp_array)
    print ('TEMP_AUTHOR_COORDs')
    print(temp_author_coords)

    args_1 = {'game': game, 'your_turn': True, 'temp_author_coords' : temp_author_coords, 'temp_opponent_coords' : temp_opponent_coords}
    args_2 = {'game': game, 'your_turn': False, 'temp_author_coords' : temp_author_coords, 'temp_opponent_coords' : temp_opponent_coords}
    #temp_array.clear()
    if request.method == 'GET':
        if game.author == request.user and game.status == Status.author_goes:
            return render(request, 'tictac/my_games.html', args_1)

        if game.opponent == request.user and game.status == Status.opponent_goes:
            return render(request, 'tictac/my_games.html', args_1)

        return render(request, 'tictac/my_games.html', args_2)

    if request.method == 'POST':
        if game.author == request.user and game.status == Status.opponent_goes:
            return redirect('tictac.views.my_games', pk = game.pk)

        if game.opponent == request.user and game.status == Status.author_goes:
            return redirect('tictac.views.my_games', pk = game.pk)

        cell = request.POST.get('cell')
        cell=''.join(cell.split())
        if cell == '':
            return redirect('tictac.views.my_games', pk = game.pk)
        y = request.POST.get('y')
        y = ''.join(y.split())
        if y == '':
            return redirect('tictac.views.my_games', pk = game.pk)
       # cell = int(cell) - 1

        print('game.author_coords: FIRST')
        print(game.author_coords)
        print('game.opponent_coords:FIRST')

        print(game.opponent_coords)
        a = game.author_coords
        b = game.opponent_coords
        author_goes = []
        opponent_goes = []

        i = 0
        j = 0
        while i<len(a):
            if a[i] == ',':
                author_goes.append(a[j:i])
                j = i + 1
            if i == len(a) - 1:
                author_goes.append(a[j:len(a)])
            i+=1

        i = 0
        while i < len(author_goes):
            author_goes[i] = int(author_goes[i])
            i += 1

        j = 0
        i = 0
        while i<len(b):
            if b[i] == ',':
                opponent_goes.append(b[j:i])
                j = i + 1
            if i == len(b) - 1:
                opponent_goes.append(b[j:len(b)])
            i+=1

        i = 0
        while i < len(opponent_goes):
            opponent_goes[i] = int(opponent_goes[i])
            i += 1

        cell = int(cell)
        y = int(y)

        if request.user == game.author:
            i = 0
            y_coords_author = []
            while i < len(author_goes):
                if author_goes[i] == cell and i % 2 == 0:
                    y_coords_author.append(author_goes[i+1])
                i+=1

            if cell in author_goes and  y in y_coords_author:
                return redirect('tictac.views.my_games', pk = game.pk)
            author_goes.append(cell)
            author_goes.append(y)

            game.status = Status.opponent_goes
            print(author_goes)
            print('caramba')
            if check(author_goes, cell, y, game.win_comb):
                game.status = Status.game_finished
            print('fiunction: check() END')

            i = 0
            while i < len(author_goes):
                author_goes[i] = str(author_goes[i])
                i += 1
            game.author_coords = ','.join(author_goes)
            print('game.author_coords:')
        else:
            i = 0
            y_coords_opponent = []
            while i < len(opponent_goes):
                if opponent_goes[i] == cell and i % 2 == 0:
                    y_coords_opponent.append(opponent_goes[i+1])
                i+=1
            if cell in opponent_goes and y in y_coords_opponent:
                return redirect('tictac.views.my_games', pk = game.pk)
            opponent_goes.append(cell)
            opponent_goes.append(y)

            print(opponent_goes)
            game.status = Status.author_goes
            if check(opponent_goes, cell, y, game.win_comb):
                game.status = Status.game_finished
            print ("check OPPONENT END")

            i = 0
            while i < len(opponent_goes):
                opponent_goes[i] = str(opponent_goes[i])
                i += 1
            game.opponent_coords=','.join(opponent_goes)
            print('game.opponent_coords:')
#        if check(board_state):
#            game.status = Status.game_finished

        print ('game.author_coords')
        print(game.author_coords)

        print('game.opponent_coords:')

        print(game.opponent_coords)
        game.save()
        return redirect('tictac.views.my_games', pk = game.pk)

def register(request):
   args={}
   args.update(csrf(request))
   args['form'] = UserCreationForm()
   if request.POST:
       newuser_form = UserCreationForm(request.POST)
       if newuser_form.is_valid():
           newuser_form.save()
           newuser = auth.authenticate(username=newuser_form.cleaned_data['username'], password=newuser_form.cleaned_data['password2'])
           return redirect("/start-game/")
       else:
           args['form'] = newuser_form
   return render(request,'registration/register.html', args)

def login(request):
   args = {}
   args.update(csrf(request))
   if request.POST:
       username = request.POST.get('username', '')
       password = request.POST.get('password', '')
       user = auth.authenticate(username=username, password=password)
       if user is not None:
           auth.login(request, user)
           return redirect('/')
       else:
           args['login_error'] = "User not found"
           return render(request, 'registration/login.html', args)
   else:
       return render(request, 'registration/login.html', args)

def logout(request):
   auth.logout(request)
   return redirect("/")

def game_list(request):
    games = Game_1.objects.filter()
    return render(request, 'tictac/game_list.html', {'games': games})

def game_join(request, pk):
    if request.user.is_authenticated():
        game = get_object_or_404(Game_1, pk=pk)
        if request.user != game.author and game.status == Status.without_opponent:
            game.opponent = request.user
            game.status = Status.author_goes
            game.save()
            return redirect('tictac.views.my_games', pk = game.pk)
        return redirect('tictac.views.game_list')
    return redirect('tictac.views.login')